from base import Action


class ApiPlayback(Action):
    action = 'api_playback'

    def __init__(self, apply_for_bridged_call, file):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'file': file
        }


class ApiUuidBridge(Action):
    action = 'api_uuid_bridge'

    def __init__(self, apply_for_bridged_call):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call
        }


class ApiUuidKill(Action):
    action = 'api_uuid_kill'

    def __init__(self, apply_for_bridged_call):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call
        }


class CancelTask(Action):
    action = 'cancel_task'

    def __init__(self, apply_for_bridged_call):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call
        }


class CopyFieldData(Action):
    action = 'copy_field_data'

    def __init__(self,from_bridged_call=False, from_field=None, to_bridged_call=False, to_field=None):
        self.data = {
            'from_bridged_call': from_bridged_call,
            'from': from_field,
            'to_bridged_call': to_bridged_call,
            'to': to_field
        }


class CreateCallToClient(Action):
    action = 'create_task'

    def __init__(self, attempt_pause, max_attempts, run_after_x_seconds, domain, destination_number, company_id, context, uuid_for_bridge, api_host, dialplan_hash):
        self.data = {
            'type': 'call_to_client',
            'attempt_pause': attempt_pause,
            'max_attempts': max_attempts,
            'run_after_x_seconds': run_after_x_seconds,
            'executor_params': {
                'domain': domain,
                'destination_number': destination_number,
                'company_id': company_id,
                'context': context,
                'uuid_for_bridge': uuid_for_bridge,
                'api_host': api_host,
                'dialplan_hash': dialplan_hash
            }
        }


class Empty(Action):
    action = 'empty'

    def __init__(self, apply_for_bridged_call):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call
        }


class RegisterCallOnLine(Action):
    action = 'register_call_on_line'

    def __init__(self, apply_for_bridged_call):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call
        }


class SendRequest(Action):
    action = 'send_request'

    def __init__(self, apply_for_bridged_call, url, method, data):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'url': url,
            'method': method,
            'data': data,
        }


class SetFieldData(Action):
    action = 'set_field_data'

    def __init__(self, apply_for_bridged_call, field, data):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'field': field,
            'data': data
        }


class UnregisterCallOnLine(Action):
    action = 'unregister_call_on_line'

    def __init__(self, apply_for_bridged_call):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call
        }


class XMLAnswer(Action):
    action = 'xml_answer'

    def __init__(self, apply_for_bridged_call, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'context': context
        }


class XMLBridge(Action):
    action = 'xml_bridge'

    def __init__(self, apply_for_bridged_call, destination, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'destination': destination,
            'context': context
        }


class XMLHangup(Action):
    action = 'xml_hangup'

    def __init__(self, apply_for_bridged_call, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'context': context
        }


class XMLPark(Action):
    action = 'xml_park'

    def __init__(self, apply_for_bridged_call, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'context': context
        }


class XMLPbxBridge(Action):
    action = 'xml_pbx_bridge'

    def __init__(self, apply_for_bridged_call, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'context': context
        }


class SetHashForBridgedCall(Action):
    action = 'set_hash_for_bridged_call'

    def __init__(self, apply_for_bridged_call, hash_str):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'hash': hash_str
        }


class XMLPlayback(Action):
    action = 'xml_playback'

    def __init__(self, apply_for_bridged_call, file, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'file': file,
            'context': context
        }


class XMLSet(Action):
    action = 'xml_set'

    def __init__(self, apply_for_bridged_call, key, value, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'key': key,
            'value': value,
            'context': context
        }


class XMLExport(XMLSet):
    action = 'xml_export'


class XMLSleep(Action):
    action = 'xml_sleep'

    def __init__(self, apply_for_bridged_call, amount, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'amount': amount,
            'context': context
        }


class XMLTransfer(Action):
    action = 'xml_transfer'

    def __init__(self, apply_for_bridged_call, destination, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'destination': destination,
            'context': context
        }


class XMLUnSet(Action):
    action = 'xml_unset'

    def __init__(self, apply_for_bridged_call, key, context):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'key': key,
            'context': context
        }


class DeleteData(Empty):
    action = 'empty'
    hash = 'XYZ'


class ApiUuidRecord(Empty):
    action = 'api_uuid_record'

    def __init__(self, apply_for_bridged_call, cmd, path, limit, domain):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'cmd': cmd,
            'path': path,
            'limit': limit,
            'domain': domain
        }


class TempKeyCreate(Empty):
    action = 'temp_key_create'

    def __init__(self, apply_for_bridged_call, name, expire):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'name': name,
            'expire': expire
        }


class TempKeyDelete(Empty):
    action = 'temp_key_delete'

    def __init__(self, apply_for_bridged_call, name):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'name': name
        }
