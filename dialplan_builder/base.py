import hashlib
import time
import uuid
from random import randint



def generate_hash():
    return str(uuid.uuid4().hex)


class Condition(object):
    checker = ''

    def __init__(self, value):
        self.value = value

    def build(self):
        return {
            'checker': self.checker,
            'value': self.value
        }


class Action(object):
    action = ''
    hash_str = None
    data = None

    def __init__(self,apply_for_bridged_call=False):
        self.data = {
            'apply_for_bridged_call': apply_for_bridged_call
        }

    def set_hash(self, hash_str=None):
        self.hash_str = hash_str
        return self

    def build(self):
        data = {}
        if self.hash_str:
            data['hash'] = self.hash_str
        if self.action:
            data['action'] = self.action
        if self.data:
            data['data'] = self.data
        return data


class Event(object):

    def __init__(self):
        self.conditions = []
        self.actions = []

    def add_condition(self, condition):
        self.conditions.append(condition)

    def add_action(self, action):
        self.actions.append(action)


class DialPlan(object):

    def __init__(self, hash_str=None):
        self.hash_str = hash_str or generate_hash()
        self.on_call_created = []
        self.on_call_in_progress = []
        self.on_call_completed = []

    def add_on_call_created(self, event):
        self.on_call_created.append(event)

    def add_on_call_in_progress(self, event):
        self.on_call_in_progress.append(event)

    def add_on_call_completed(self, event):
        self.on_call_completed.append(event)

    def _build_events(self, events):
        _events = []
        for event in events:
            data = {}
            if event.conditions:
                data['conditions'] = []
                for condition in event.conditions:
                    data['conditions'].append(condition.build())
            if event.actions:
                data['actions'] = []
                for action in event.actions:
                    data['actions'].append(action.build())
            _events.append(data)
        return _events

    def build(self):
        data = {}
        if self.hash_str:
            data['hash'] = self.hash_str

        self.on_call_created = self._build_events(self.on_call_created)
        if self.on_call_created:
            data['call_created'] = self.on_call_created

        self.on_call_in_progress = self._build_events(self.on_call_in_progress)
        if self.on_call_in_progress:
            data['call_in_progress'] = self.on_call_in_progress

        self.on_call_completed = self._build_events(self.on_call_completed)
        if self.on_call_completed:
            data['call_completed'] = self.on_call_completed
        return data