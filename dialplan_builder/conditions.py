from base import Condition


class EquallyDestinationNumber(Condition):
    checker = 'equally_destination_number'

    def __init__(self, value, apply_for_bridged_call=False):
        self.value = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'value': value
        }


class EquallyCallerNumber(Condition):
    checker = 'equally_caller_number'

    def __init__(self, value, apply_for_bridged_call=False):
        self.value = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'value': value
        }


class EquallyState(Condition):
    checker = 'equally_state'

    def __init__(self, value, apply_for_bridged_call=False):
        self.value = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'value': value
        }


class EquallyApplication(Condition):
    checker = 'equally_application'

    def __init__(self, value, apply_for_bridged_call=False):
        self.value = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'value': value
        }


class EquallyData(Condition):
    checker = 'equally_data'

    def __init__(self, value, apply_for_bridged_call=False):
        self.value = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'value': value
        }


class EquallyBridgedCallUuid(Condition):
    checker = 'equally_equally_bridged_call_uuid'

    def __init__(self, value, apply_for_bridged_call=False):
        self.value = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'value': value
        }


class TempKeyIsAvailable(Condition):
    checker = 'temp_key_is_available'

    def __init__(self, value, name, apply_for_bridged_call=False):
        self.value = {
            'apply_for_bridged_call': apply_for_bridged_call,
            'value': value,
            'name': name
        }


class TaskIsAvailable(Condition):
    checker = 'task_is_available'