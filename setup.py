from setuptools import setup


setup(
    name='dialplan_builder',
    version='0.1',
    packages=['dialplan_builder'],
    url='https://bitbucket.org/taxicloudservice/dialplan_builder',
    license='GPLv2',
    author='taxicloudservice',
    author_email='info@ligataxi.com',
    description='Dialplan builder',
)
